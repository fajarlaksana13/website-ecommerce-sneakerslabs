<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard','AdminController@dashboard')->middleware('admin');
Route::get('/transaksi','AdminTransaksiController@transaksi')->middleware('admin');
Route::get('/produk','AdminProdukController@produk')->middleware('admin');
Route::get('/user','AdminUserController@user')->middleware('admin');
Route::get('/berita','AdminBeritaController@berita')->middleware('admin');
Route::get('/banner','AdminBannerController@banner')->middleware('admin');
Route::get('/detail/{id_produk}','UserDetailController@detail')->middleware('admin');


Route::get('/about','UserAboutController@about');
Route::get('/addtocart','UserCartController@addtocart')->name('cart.add');
Route::delete('/remove', 'UserCartController@remove')->name('cart.remove');
Route::delete('/removeall', 'UserCartController@removeall')->name('cart.removeall');


Route::get('/index','UserCartController@index');
Route::get('/checkout','UserCheckoutController@checkout');
Route::post('/checkout2','UserCheckoutController@prosesShipping')->name('checkout2.add');
Route::post('/checkout3','UserCheckoutController@kirim')->name('checkout3.kirim');
Route::post('/checkout','UserCheckoutController@store')->name('checkout3.store');


Route::get('/getprovince','UserCheckoutController@getprovince');
Route::get('/getcity','UserCheckoutController@getcity');


Route::get('/berita', 'AdminBeritaController@index')->name('berita.index');
Route::post('/berita', 'AdminBeritaController@store')->name('berita.store');
Route::get('/tambahdataBerita', 'AdminBeritaController@create');
Route::get('/berita/{id_berita}/edit', 'AdminBeritaController@edit')->name('berita.edit');
Route::patch('/berita/{id_berita}','AdminBeritaController@update')->name('berita.update');
Route::delete('/berita/{id_berita}', 'AdminBeritaController@destroy')->name('berita.destroy');
Route::get('/blogIsi/{id_berita}/detail', 'AdminBeritaController@detail')->name('user.blogIsi');
Route::get('/detail/{id_produk}/detail', 'AdminProdukController@detail')->name('user.detail');

Route::get('/banner', 'AdminBannerController@index')->name('banner.index');
Route::post('/banner', 'AdminBannerController@store')->name('banner.store');
Route::get('/tambahdataBanner', 'AdminBannerController@create');
Route::get('/banner/{id_banner}/edit', 'AdminBannerController@edit')->name('banner.edit');
Route::patch('/banner/{id_banner}','AdminBannerController@update')->name('banner.update');
Route::delete('/banner/{id_banner}', 'AdminBannerController@destroy')->name('banner.destroy');

Route::get('/produk', 'AdminProdukController@index')->name('produk.index');
Route::post('/produk', 'AdminProdukController@store')->name('produk.store');
Route::get('/tambahdataProduk', 'AdminProdukController@create');
Route::get('/produk/{id_produk}/edit', 'AdminProdukController@edit')->name('produk.edit');
Route::patch('/produk/{id_produk}','AdminProdukController@update')->name('produk.update');
Route::delete('/produk/{id_produk}', 'AdminProdukController@destroy')->name('produk.destroy');
Route::get('/produk/{id_produk}/detail', 'AdminProdukController@detail')->name('admin.produk');

Route::get('/kategori', 'AdminKategoriController@index')->name('kategori.index');
Route::post('/kategori', 'AdminKategoriController@store')->name('kategori.store');
Route::get('/tambahdataKategori', 'AdminKategoriController@create');
Route::get('/kategori/{id}/edit', 'AdminKategoriController@edit')->name('kategori.edit');
Route::patch('/kategori/{id}','AdminKategoriController@update')->name('kategori.update');
Route::delete('/kategori/{id}', 'AdminKategoriController@destroy')->name('kategori.destroy');

Route::get('/isi', 'UserHomeIsiController@index')->name('isi');
Route::get('/blog', 'UserBlogController@index')->name('blog');
Route::get('/produkuser', 'UserProdukController@index')->name('produkuser');


Auth::routes();

Route::get('/home', 'UserHomeIsiController@index')->name('home');
Route::get('/', 'UserHomeIsiController@index');


