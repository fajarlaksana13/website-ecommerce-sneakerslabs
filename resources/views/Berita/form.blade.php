<html>
<head>
    <title>Form Registrasi Data Berita</title>
    <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
</head>
<body>
<div class="container">
    @if(session('error'))
        <div class="alert alert-error">
            {{ session('error') }}
        </div>
    @endif
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong> Perhatian </strong><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <h1>Data berita</h1>
    <form action="{{ url('berita', @$berita->id_berita) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @if(!empty($berita))
            @method('PATCH')
        @endif
        <div class="form-group">
            <label for="id_berita">ID Berita</label>
            <input type="text" class="form-control" name="id_berita" value="{{ old('id_berita', @$berita->id_berita) }}"
                   placeholder="Masukkan ID" readonly>
            <label for="judul">Judul</label>
            <input type="text" class="form-control" name="judul" value="{{ old('judul', @$berita->judul) }}"
                   placeholder="Masukkan Judul">
            <label for="deskripsi">Deskripsi Pendek</label>
            <textarea class="form-control" name="deskripsi" cols="20" rows="2" id="placeOfDeath"
            value="{{ old('deskripsi', @$berita->deskripsi) }}" placeholder="Masukkan Deskripsi Pendek"></textarea>
            <label for="isi">Deskripsi Panjang</label>
            <textarea class="form-control" name="isi" cols="50" rows="10" id="placeOfDeath"
            value="{{ old('isi', @$berita->isi) }}" placeholder="Masukkan Deskripsi Panjang"></textarea>
        </div>
        
        <div class="form-group">
            <label for="email">Cover</label>
            <div>
                <img width="100" height="100"/>
                <input type="file" class="uploads form-control" name="gambar">
            </div>
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-primary" value="SAVE">
        </div>
</body>
</html>