<html>
<head>
    <title>Form Edit Data Berita</title>
    <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
</head>
<body>
<div class="container">
    @if(session('error'))
        <div class="alert alert-error">
            {{ session('error') }}
        </div>
    @endif
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong> Perhatian </strong><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <h1>Data Berita</h1>
    <form action="{{route('berita.update',['id_berita' => @$berita->id_berita])}}" method="POST" enctype="multipart/form-data">
        {{--{{ url('berita', ['id_berita' => @$berita->id_berita]).'/update' }}--}}
        
        @csrf
        @method('PATCH')
        <div class="form-group">
            <label for="id_berita">ID Berita</label>
            <input type="text" class="form-control" name="id_berita" value="{{ old('id_berita', @$berita->id_berita) }}"
                   placeholder="Masukkan ID" readonly>
            <label for="judul">Judul</label>
            <input type="text" class="form-control" name="judul" value="{{ old('judul', @$berita->judul) }}"
                   placeholder="Masukkan Judul">
            <label for="deskripsi">Deskripsi Pendek</label>
            <textarea class="form-control" name="deskripsi" cols="20" rows="2" id="placeOfDeath"
           placeholder="Masukkan Deskripsi Pendek"> {{ old('deskripsi', @$berita->deskripsi) }} </textarea>
            <label for="isi">Deskripsi Panjang</label>
            <textarea class="form-control" name="isi" cols="50" rows="10" id="placeOfDeath"
             placeholder="Masukkan Deskripsi Panjang">{{ old('isi', @$berita->isi) }}</textarea>
        </div>
        <div class="form-group">
            <label for="gambar">Foto</label>
            <div>
                @if(@$berita->gambar)
                    <img src="{{ url('uploads/'.@$berita->gambar) }}" class="img-thumbnail" width="150px"
                         alt="image"/>
                @endif
                <input type="file" class="form-control-file mt-1" name="gambar" >
            </div>
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-primary" value="SAVE">
        </div>
</body>
</html>