<html>
<head>
    <title>Form Edit Data Produk</title>
    <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
</head>
<body>
<div class="container">
    @if(session('error'))
        <div class="alert alert-error">
            {{ session('error') }}
        </div>
    @endif
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong> Perhatian </strong><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <h1>Data Produk</h1>
    <form action="{{route('produk.update',['id_produk' => @$produk->id_produk])}}" method="POST" enctype="multipart/form-data">
        {{--{{ url('berita', ['id_berita' => @$berita->id_berita]).'/update' }}--}}
        
        @csrf
        @method('PATCH')
        <div class="form-group">
            <label for="id_produk">ID Produk</label>
            <input type="text" class="form-control" name="id_produk" value="{{ old('id_produk', @$produk->id_produk) }}"
                   placeholder="Masukkan ID" readonly>
            <label for="nama_produk">Nama Produk</label>
            <input type="text" class="form-control" name="nama_produk" value="{{ old('nama_produk', @$produk->nama_produk) }}"
                   placeholder="Masukkan Nama">
            <label for="deskripsi">Deskripsi</label>
            <textarea class="form-control" name="deskripsi" cols="20" rows="2" id="placeOfDeath"
           placeholder="Masukkan Deskripsi"> {{ old('deskripsi', @$produk->deskripsi) }} </textarea>
            <label for="harga">Harga</label>
            <textarea class="form-control" name="harga" cols="50" rows="10" id="placeOfDeath"
             placeholder="Masukkan Harga Produk">{{ old('harga', @$produk->harga) }}</textarea>
            <label for="kategori_id">Kategori</label>
            <input type="text" class="form-control" name="kategori_id" value="{{ old('kategori_id', @$produk->kategori_id) }}"
                   placeholder="(1)Women,(2)Men"></textarea>
        </div>
        <div class="form-group">
            <label for="gambar">Foto</label>
            <div>
                @if(@$produk->gambar)
                    <img src="{{ url('uploads/'.@$produk->gambar) }}" class="img-thumbnail" width="150px"
                         alt="image"/>
                @endif
                <input type="file" class="form-control-file mt-1" name="gambar" >
            </div>
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-primary" value="SAVE">
        </div>
</body>
</html>