<html>
<head>
    <title>Form Registrasi Data Produk</title>
    <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
</head>
<body>
<div class="container">
    @if(session('error'))
        <div class="alert alert-error">
            {{ session('error') }}
        </div>
    @endif
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong> Perhatian </strong><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <h1>Data Produk</h1>
    <form action="{{ url('produk', @$produk->id_produk) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @if(!empty($produk))
            @method('PATCH')
        @endif
        <div class="form-group">
            <label for="id_produk">ID produk</label>
            <input type="text" class="form-control" name="id_produk" value="{{ old('id_produk', @$produk->id_produk) }}"
                   placeholder="Masukkan ID" readonly>
            <label for="nama_produk">Nama Produk</label>
            <input type="text" class="form-control" name="nama_produk" value="{{ old('nama_produk', @$produk->nama_produk) }}"
                   placeholder="Masukkan Nama Produk">
            <label for="deskripsi">Deskripsi</label>
            <textarea class="form-control" name="deskripsi" cols="20" rows="10" id="placeOfDeath"
            value="{{ old('deskripsi', @$produk->deskripsi) }}" placeholder="Masukkan Deskripsi"></textarea>
            <label for="harga">Harga</label>
            <input type="text" class="form-control" name="harga" value="{{ old('harga', @$produk->harga) }}"
                   placeholder="Masukkan Harga"></textarea>
            <label for="kategori_id">Kategori</label>
            <input type="text" class="form-control" name="kategori_id" value="{{ old('kategori_id', @$produk->kategori_id) }}"
                   placeholder="(1)Women,(2)Men"></textarea>
        </div>
        <div class="form-group">
            <label for="email">Cover</label>
            <div>
                <img width="100" height="100"/>
                <input type="file" class="uploads form-control" name="gambar">
            </div>
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-primary" value="SAVE">
        </div>
</body>
</html>