<html>
<head>
    <title>Form Edit Data kategori</title>
    <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
</head>
<body>
<div class="container">
    @if(session('error'))
        <div class="alert alert-error">
            {{ session('error') }}
        </div>
    @endif
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong> Perhatian </strong><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <h1>Data kategori</h1>
    <form action="{{route('kategori.update',['id' => @$kategori->id])}}" method="POST" enctype="multipart/form-data">
        {{--{{ url('kategori', ['id' => @$kategori->id]).'/update' }}--}}
        
        @csrf
        @method('PATCH')
        <div class="form-group">
            <label for="id">ID kategori</label>
            <input type="text" class="form-control" name="id" value="{{ old('id', @$kategori->id) }}"
                   placeholder="Masukkan ID" readonly>
            <label for="nama_kategori">Nama Kategori</label>
            <input type="text" class="form-control" name="nama_kategori" value="{{ old('nama_kategori', @$kategori->nama_kategori) }}"
                   placeholder="Masukkan nama kategori">
            <label for="status">Status</label>
            <textarea class="form-control" name="status" cols="20" rows="2" id="placeOfDeath"
           placeholder="Masukkan Deskripsi Pendek"> {{ old('status', @$kategori->status) }} </textarea
        </div>
        <br>
        <div class="form-group">
            <input type="submit" class="btn btn-primary" value="SAVE">
        </div>
</body>
</html>