<html>
<head>
    <title>Form Registrasi Data kategori</title>
    <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
</head>
<body>
<div class="container">
    @if(session('error'))
        <div class="alert alert-error">
            {{ session('error') }}
        </div>
    @endif
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong> Perhatian </strong><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <h1>Data kategori</h1>
    <form action="{{ url('kategori', @$kategori->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @if(!empty($kategori))
            @method('PATCH')
        @endif
        <div class="form-group">
            <label for="id">ID kategori</label>
            <input type="text" class="form-control" name="id" value="{{ old('id', @$kategori->id) }}"
                   placeholder="Masukkan ID" readonly>
            <label for="nama_kategori">Nama Kategori</label>
            <input type="text" class="form-control" name="nama_kategori" value="{{ old('nama_kategori', @$kategori->nama_kategori) }}"
                   placeholder="Masukkan Nama Kategori">
            <label for="status">Status</label>
            <textarea class="form-control" name="status" cols="20" rows="2" id="placeOfDeath"
            value="{{ old('status', @$kategori->status) }}" placeholder="Masukkan Status"></textarea>
        </div>

        <div class="form-group">
            <input type="submit" class="btn btn-primary" value="SAVE">
        </div>
</body>
</html>