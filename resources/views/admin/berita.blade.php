@extends('layout.master')

@section('content')
<!-- start: content -->
{{--<div class="col-md-12" style="padding:20px;">--}}
{{--    <div id="content">--}}
{{--        <div class="col-md-12 top-20 padding-0">--}}
            <div class="">
                <div class="panel">
                    <div class="panel-heading"><h3>Data Berita </h3></div>
                    <div class="panel-body">
                        <div class="responsive-table">
                            <a class="btn btn-primary" type="submit" href="{{ url('/tambahdataBerita') }}"> Tambah
                                Data </a>
                            <br>
                            {{$berita->links()}}
                            <table id="datatables-example" class="table table-striped table-bordered" width="100%"
                                   cellspacing="0">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Gambar</th>
                                    <th>ID Berita</th>
                                    <th>Judul</th>
                                    <th>Deskripsi</th>
                                    <th>Isi</th>

                                    <th colspan="2" width="auto">
                                        <center>Aksi</center>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($berita as $row)
                                    <tr>
                                        <td> {{ isset($i) ? ++$i : $i = 1 }} </td>
                                        <td class="py-1">
                                            @if($row->gambar)
                                                <img src="{{ url('uploads/'. $row->gambar) }}"
                                                     class="img-thumbnail" alt="image" width="225px"/>
                                            @else
                                                <img src="{{ url('image/Produk/default.png') }}" alt="image"/>
                                            @endif
                                        </td>
                                        <td> {{ $row->id_berita }} </td>
                                        <td> {{ $row->judul }} </td>
                                        <td> {!! \Illuminate\Support\Str::words($row->deskripsi, 10, '...') !!}{!! \Illuminate\Support\Str::words($row->deskripsi, 10, '...') !!} </td>
                                        <td> {!! \Illuminate\Support\Str::words($row->isi, 20, '...') !!}
                                        <td>
                                            <a class="btn btn-success"
                                               href="{{ url('berita/' . $row->id_berita . '/edit') }}">Edit</a>
                                        </td>
                                        <td>
                                            <form action="{{ url('/berita', $row->id_berita) }}" method="POST">
                                                @method('DELETE')
                                                @csrf
                                                <button class="btn btn-danger" type="submit">Delete</button>
                                            </form>
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
{{--<!-- end: content -->--}}
{{--</div>--}}
@endsection