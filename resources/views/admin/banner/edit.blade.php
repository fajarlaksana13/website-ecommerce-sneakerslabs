<html>
<head>
    <title>Form Edit Data banner</title>
    <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
</head>
<body>
<div class="container">
    @if(session('error'))
        <div class="alert alert-error">
            {{ session('error') }}
        </div>
    @endif
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong> Perhatian </strong><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <h1>Data banner</h1>
    <form action="{{route('banner.update',['id_banner' => @$banner->id_banner])}}" method="POST" enctype="multipart/form-data">
        {{--{{ url('banner', ['id_banner' => @$banner->id_banner]).'/update' }}--}}
        
        @csrf
        @method('PATCH')
        <div class="form-group">
            <label for="id_banner">ID banner</label>
            <input type="text" class="form-control" name="id_banner" value="{{ old('id_banner', @$banner->id_banner) }}"
                   placeholder="Masukkan ID" readonly>
            <label for="judul">Judul</label>
            <input type="text" class="form-control" name="judul" value="{{ old('judul', @$banner->judul) }}"
                   placeholder="Masukkan Judul">
            <label for="deskripsi">Deskripsi</label>
            <textarea class="form-control" name="deskripsi" cols="20" rows="2" id="placeOfDeath"
           placeholder="Masukkan Deskripsi Pendek"> {{ old('deskripsi', @$banner->deskripsi) }} </textarea>
        </div>
        <div class="form-group">
            <label for="gambar">Foto</label>
            <div>
                @if(@$banner->gambar)
                    <img src="{{ url('uploads/'.@$banner->gambar) }}" class="img-thumbnail" width="150px"
                         alt="image"/>
                @endif
                <input type="file" class="form-control-file mt-1" name="gambar" >
            </div>
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-primary" value="SAVE">
        </div>
</body>
</html>