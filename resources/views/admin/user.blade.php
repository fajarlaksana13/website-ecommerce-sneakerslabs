@extends('layout.master')

@section('content')


  <div class="">
    <div class="panel">
      <div class="panel-heading"><h3>Data User</h3></div>
      <div class="panel-body">
        <div class="responsive-table">
{{--          <a class="btn btn-primary" type="submit" href="{{ url('/tambahdataProduk') }}"> Tambah--}}
{{--            Data </a>--}}
{{--          <br>--}}
{{--          {{$produk->links()}}--}}
          <table id="datatables-example" class="table table-striped table-bordered" width="100%"
                 cellspacing="0">
            <thead>
            <tr>
              <th>No</th>
              <th>Id</th>
              <th>Nama</th>
              <th>Email</th>
              <th>Role</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($users as $row)
              <tr>
                <td>{{ isset($i) ? ++$i : $i = 1 }}</td>
                <td>{{ $row->id }}</td>
                <td>{{ $row->name }}</td>
                <td>{{ $row->email }}</td>
                <td>{{ $row->role }}</td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

@endsection