@extends('layout.master')

@section('content')
    <!-- start: content -->
{{--    <div class="col-md-12" style="padding:20px;">--}}
{{--        <div id="content">--}}
{{--            <div class="col-md-12 top-20 padding-0">--}}
                <div class="">
                    <div class="panel">
                        <div class="panel-heading"><h3>Data kategori </h3></div>
                        <div class="panel-body">
                            <div class="responsive-table">
                                <a class="btn btn-primary" type="submit" href="{{ url('/tambahdataKategori') }}"> Tambah
                                    Data </a>
                                <br>
                                {{$kategori->links()}}
                                <table id="datatables-example" class="table table-striped table-bordered" width="100%"
                                       cellspacing="0">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>ID</th>
                                        <th>Nama kategori</th>
                                        <th>Status</th>
                                        <th>Produk</th>
                                        <th>Jumlah</th>

                                        <th colspan="2" width="auto">
                                            <center>Aksi</center>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($kategori as $row)
                                        <tr>
                                            <td> {{ isset($i) ? ++$i : $i = 1 }} </td>
                                            <td> {{ $row->id_kategori }} </td>
                                            <td> {{ $row->nama_kategori }} </td>
                                            <td> {{ $row->status }} </td>
                                            <td>
                                                @foreach($row->relasiProduk as $test)
                                                    {{$test->nama_produk}},
                                                @endforeach
                                            </td>
                                            <td>{{$row->relasiProduk->count()}}</td>
                                            <td>
                                                <a class="btn btn-success"
                                                   href="{{ url('kategori/' . $row->id . '/edit') }}">Edit</a>
                                            </td>
                                            <td>
                                                <form action="{{ url('/kategori', $row->id) }}" method="POST">
                                                    @method('DELETE')
                                                    @csrf
                                                    <button class="btn btn-danger" type="submit">Delete</button>
                                                </form>
                                            </td>

                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <!-- end: content -->--}}
{{--    </div>--}}
@endsection    