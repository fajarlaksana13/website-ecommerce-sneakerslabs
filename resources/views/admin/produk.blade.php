@extends('layout.master')

@section('content')
    <!-- start: content -->
{{--    <div class="col-md-12" style="padding:20px;">--}}
{{--        <div id="content">--}}
{{--            <div class="col-md-12 ">--}}
                <div class="">
                    <div class="panel">
                        <div class="panel-heading"><h3>Data Produk </h3></div>
                        <div class="panel-body">
                            <div class="responsive-table">
                                <a class="btn btn-primary" type="submit" href="{{ url('/tambahdataProduk') }}"> Tambah
                                    Data </a>
                                <br>
                                {{$produk->links()}}
                                <table id="datatables-example" class="table table-striped table-bordered" width="100%"
                                       cellspacing="0">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Gambar</th>
                                        <th>ID</th>
                                        <th>Nama Produk</th>
                                        <th>Deskripsi</th>
                                        <th>Harga</th>
                                        <th>Kategori</th>

                                        <th colspan="2" width="auto">
                                            <center>Aksi</center>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($produk as $row)
                                        <tr>
                                            <td> {{ isset($i) ? ++$i : $i = 1 }} </td>
                                            <td class="py-1">
                                                @if($row->gambar)
                                                    <img src="{{ url('uploads/'. $row->gambar) }}"
                                                         class="img-thumbnail" alt="image" width="225px"/>
                                                @else
                                                    <img src="{{ url('image/Produk/default.png') }}" alt="image"/>
                                                @endif
                                            </td>
                                            <td> {{ $row->id_produk }} </td>
                                            <td> {{ $row->nama_produk }} </td>
                                            <td> {{ $row->deskripsi }} </td>
                                            <td> {{ $row->harga }} </td>
                                            <td> {{ $row->kategori_id  }} </td>
                                            <td>
                                                <a class="btn btn-success"
                                                   href="{{ url('produk/' . $row->id_produk . '/edit') }}">Edit</a>
                                            </td>
                                            <td>
                                                <form action="{{ url('/produk', $row->id_produk) }}" method="POST">
                                                    @method('DELETE')
                                                    @csrf
                                                    <button class="btn btn-danger" type="submit">Delete</button>
                                                </form>
                                            </td>

                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
    <!-- end: content -->
@endsection    