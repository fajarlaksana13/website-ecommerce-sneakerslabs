@extends('user.home')

@section('content')
    <br>
    <br>
    <section class="blog_area single-post-area section_gap">
        <div class="container">
            <div class="row">
                <div class="col-lg-20 posts-list">
                    <div class="single-post row">
                        <div class="col-lg-25">
                            <div class="feature-img ml-5">
                                <img class="img-fluid" src="{{url('uploads/').'/'.$berita['berita']->gambar}}" alt="">
                            </div>
                            <br>
                        </div>
                        <div class="col-lg-20 col-md-20 blog_details">
                            <h2>{{$berita['berita']->judul}}</h2>
                            <p class="excert">
                                {{$berita['berita']->deskripsi}}
                            </p>
                            <p>
                                {{$berita['berita']->isi}}
                            </p>
                        </div>
                    </div>
                </div>
        </div>
    </section>
@endsection