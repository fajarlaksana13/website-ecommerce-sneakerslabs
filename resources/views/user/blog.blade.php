@extends('user.home')

@section('content')
    <section class="banner-area organic-breadcrumb">
        <div class="container">
            <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                <div class="col-first">
                    <h1>News Page</h1>
                    <nav class="d-flex align-items-center">
                        <a href="index.html">Home<span class="lnr lnr-arrow-right"></span></a>
                        <a href="category.html">News</a>
                    </nav>
                </div>
            </div>
        </div>
    </section>

<!--================Blog Area =================-->
<section class="blog_area">
    <div class="container mt-5 ml-10">
        <div class="row">
            <div class="col-lg-20">
                <div class="blog_left_sidebar">
                    @foreach($berita as $row)
                    <article class="row blog_item">
                        <div class="col-md-30">
                            <div class="blog_post">
                                <a href="{{url('blogIsi/'.$row->id_berita.'/detail')}}">
                                <img src="{{url('uploads/').'/'.$row->gambar}}" alt="">
                                </a>
                                <div class="blog_details">
                                    <a href="{{url('blogIsi/'.$row->id_berita.'/detail')}}">
                                        <h2>{{$row->judul}}</h2>
                                    </a>
                                    <p>{{$row->deskripsi}}</p>
                                    <a href="{{url('blogIsi/'.$row->id_berita.'/detail')}}" class="white_bg_btn">View More</a>
                                </div>
                            </div>
                        </div>
                    </article>
                    @endforeach
                    <nav class="blog-pagination justify-content-center d-flex">
                        <ul class="pagination">
                            {{$berita->links()}}
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================Blog Area =================-->
@endsection