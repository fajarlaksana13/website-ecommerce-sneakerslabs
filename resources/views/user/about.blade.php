@extends('user.home')

@section('content')
    <section class="banner-area organic-breadcrumb">
        <div class="container">
            <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                <div class="col-first">
                    <h1>About Page</h1>
                    <nav class="d-flex align-items-center">
                        <a href="index.html">Home<span class="lnr lnr-arrow-right"></span></a>
                        <a href="category.html">About</a>
                    </nav>
                </div>
            </div>
        </div>
    </section>

    <section class="sample-text-area">
        <div class="container">
            <h3 class="text-heading">HISTORY</h3>
            <p class="sample-text">
                In 1963, the F.W. Woolworth Company purchased the Kinney Shoe Corporation and operated it as a subsidiary.
                In the 1960s, Kinney branched into specialty shoe stores, including Stylco in 1967, Susie Casuals in 1968,
                and Foot Locker on September 12, 1974. The first Foot Locker opened in the Puente Hills Mall in City of Industry, California,
                and still is open today.[6] Woolworth also diversified its portfolio of specialty stores in the 1980s, including Afterthoughts,
                Northern Reflections, Rx Place, and Champs Sports. By 1989, the company was pursuing an aggressive strategy of multiple specialty
                store formats targeted at enclosed shopping malls. The idea was that if a particular concept failed at a given mall, the company
                could quickly replace it with a different concept. The company aimed for 10 stores in each of the country's major shopping malls,
                but this never came to pass as Woolworth never developed that many successful specialty store formats.
            </p>
        </div>
    </section>

    <div class="whole-wrap pb-100">
        <div class="container">
            <div class="section-top-border">
                <h3 class="mb-30">Block Quotes</h3>
                <div class="row">
                    <div class="col-lg-12">
                        <blockquote class="generic-blockquote">
                            “Recently, the US Federal government banned online casinos from operating in America by making it illegal to
                            transfer money to them through any US bank or payment system. As a result of this law, most of the popular
                            online casino networks such as Party Gaming and PlayTech left the United States. Overnight, online casino
                            players found themselves being chased by the Federal government. But, after a fortnight, the online casino
                            industry came up with a solution and new online casinos started taking root. These began to operate under a
                            different business umbrella, and by doing that, rendered the transfer of money to and from them legal. A major
                            part of this was enlisting electronic banking systems that would accept this new clarification and start doing
                            business with me. Listed in this article are the electronic banking”
                        </blockquote>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <br>
    </div>
@endsection