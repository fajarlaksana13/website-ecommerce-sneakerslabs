@extends('user.home')

@section('content')
    <section class="banner-area organic-breadcrumb">
        <div class="container">
            <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                <div class="col-first">
                    <h1>Shopping Cart</h1>
                    <nav class="d-flex align-items-center">
                        <a href="index.html">Home<span class="lnr lnr-arrow-right"></span></a>
                        <a href="category.html">Cart</a>
                    </nav>
                </div>
            </div>
        </div>
    </section>

    <section class="cart_area">
        <div class="container">
            <div class="cart_inner">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">Product</th>
                            <th scope="col">Price</th>
                            <th scope="col">Quantity</th>
                            <th scope="col">Total</th>
                            <th scope="col"></th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $total = 0 ?>

                        @if(session('cart'))
                            @foreach((array) session('cart') as $id => $details)
                                <!--                                --><?php //dd(session('cart')) ?>
<!--                                --><?php //dd($details) ?>
                                <?php $total += $details['harga'] * $details['quantity'] ?>
                                <tr>
                                    <td>
                                        <div class="media">
                                            <div class="d-flex">
                                                <img src="{{url('uploads/').'/'.$details['gambar']}}" width="100px" alt="">
                                            </div>
                                            <div class="media-body">
                                                <p>{{$details['nama_produk']}}</p>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <h5>{{App\Http\Controllers\UserProdukController::rupiah($details['harga'])}}</h5>
                                    </td>
                                    <td>
                                        <div class="product_count">
                                            <input type="text" name="qty" id="sst" maxlength="12" value="{{$details['quantity']}}"
                                                   title="Quantity:" class="input-text qty">

                                        </div>
                                    </td>
                                    <td width="100px">
                                        <div>
                                            <h5>{{App\Http\Controllers\UserProdukController::rupiah($details['harga'] * $details['quantity'])}}</h5>
                                        </div>
                                    </td>
                                    <td>

                                    </td>
                                    <td>
                                        <a class="remove-from-cart" data-id="{{$id}}" href="" style="font-size: 25px; color: #ff2222;">X</a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        <tr class="bottom_button">
                            <td>
                                <form action="{{route('cart.removeall')}}" method="post">
                                    @method('DELETE')
                                    @csrf
                                    <button class="gray_btn" type="submit" style="background-color: #ff9e7b">Delete All</button>
                                </form>
                            </td>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>

                            </td>
                        </tr>
                        <tr>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>
                                <h5>Subtotal</h5>
                            </td>
                            <td>
                                <h5>{{App\Http\Controllers\UserProdukController::rupiah($total)}}</h5>
                            </td>
                        </tr>
                        <tr class="out_button_area">
                            <td>

                            </td>

                            <td>
                                <div class="checkout_btn_inner d-flex align-items-center">
                                    <a class="gray_btn" href="{{url('produkuser')}}">Continue Shopping</a>
                                    <a class="primary-btn" href="{{url('checkout')}}">Proceed to checkout</a>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>



    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script type="text/javascript">

        $(".update-cart").click(function (e) {
            e.preventDefault();

            var ele = $(this);

            var parent_row = ele.parents("tr");

            var quantity = parent_row.find(".quantity").val();

            var product_subtotal = parent_row.find("span.product-subtotal");

            var cart_total = $(".cart-total");

            var loading = parent_row.find(".btn-loading");

            loading.show();

            $.ajax({
                url: '{{ url('update-cart') }}',
                method: "patch",
                data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id"), quantity: quantity},
                dataType: "json",
                success: function (response) {

                    loading.hide();

                    $("span#status").html('<div class="alert alert-success">'+response.msg+'</div>');

                    $("#header-bar").html(response.data);

                    product_subtotal.text(response.subTotal);

                    cart_total.text(response.total);
                }
            });
        });

        $(".remove-from-cart").click(function (e) {
            e.preventDefault();

            var ele = $(this);

            var parent_row = ele.parents("tr");

            var cart_total = $(".cart-total");

            if(confirm("Are you sure")) {
                $.ajax({
                    url: '{{ route('cart.remove') }}',
                    method: "DELETE",
                    data: {_token: '{{ csrf_token() }}', id_produk: ele.attr("data-id")},
                    dataType: "json",
                    success: function (response) {

                        // parent_row.remove();
                        //
                        // $("span#status").html('<div class="alert alert-success">'+response.msg+'</div>');
                        //
                        // $("#header-bar").html(response.data);

                        // cart_total.text(response.total);
                        window.location.reload();
                    },
                    error: function () {
                        alert("Gagal Memasukan Cart");
                    }
                });
            }
        });

    </script>

@endsection