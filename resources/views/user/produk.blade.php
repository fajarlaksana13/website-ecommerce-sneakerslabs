@extends('user.home')

@section('content')
    <!-- Start Banner Area -->
    <section class="banner-area organic-breadcrumb">
        <div class="container">
            <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                <div class="col-first">
                    <h1>Sneakers Shop Page</h1>
                    <nav class="d-flex align-items-center">
                        <a href="index.html">Home<span class="lnr lnr-arrow-right"></span></a>
                        <a href="#">Shop<span class="lnr lnr-arrow-right"></span></a>
                        <a href="category.html">Sneakers</a>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <!-- End Banner Area -->
    <br>
    <br>
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-4 col-md-5">
                <div class="sidebar-categories">
                    <div class="head">Browse Categories</div>
                    <ul class="main-categories">
                        <li class="main-nav-list"><a data-toggle="collapse" href="#fruitsVegetable"
                                                     aria-expanded="false" aria-controls="fruitsVegetable"><span
                                        class="lnr lnr-arrow-right"></span>Shoes For Men<span class="number">(53)</span></a>
                            <ul class="collapse" id="fruitsVegetable" data-toggle="collapse" aria-expanded="false"
                                aria-controls="fruitsVegetable">
                                <li class="main-nav-list child"><a href="#">All<span class="number">(13)</span></a></li>
                            </ul>
                        </li>

                        <li class="main-nav-list"><a data-toggle="collapse" href="#meatFish" aria-expanded="false"
                                                     aria-controls="meatFish"><span
                                        class="lnr lnr-arrow-right"></span>Shoes For Women<span
                                        class="number">(53)</span></a>
                            <ul class="collapse" id="meatFish" data-toggle="collapse" aria-expanded="false"
                                aria-controls="meatFish">
                                <li class="main-nav-list child"><a href="#">All<span class="number">(13)</span></a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="sidebar-filter mt-50">
                    <div class="top-filter-head">Product Filters</div>
                    <div class="common-filter">
                        <div class="head">Brands</div>
                        <form action="#">
                            <ul>
                                <li class="filter-list"><input class="pixel-radio" type="radio" id="apple" name="brand"><label
                                            for="apple">NIKE<span>(29)</span></label></li>
                                <li class="filter-list"><input class="pixel-radio" type="radio" id="asus"
                                                               name="brand"><label
                                            for="asus">ADIDAS<span>(29)</span></label></li>
                                <li class="filter-list"><input class="pixel-radio" type="radio" id="gionee"
                                                               name="brand"><label
                                            for="gionee">CONVERSE<span>(19)</span></label></li>
                                <li class="filter-list"><input class="pixel-radio" type="radio" id="micromax"
                                                               name="brand"><label for="micromax">VANS<span>(19)</span></label>
                                </li>
                                <li class="filter-list"><input class="pixel-radio" type="radio" id="samsung"
                                                               name="brand"><label for="samsung">LE COQ<span>(19)</span></label>
                                </li>
                            </ul>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-xl-9 col-lg-8 col-md-7">
                <!-- Start Filter Bar -->
                <div class="filter-bar d-flex flex-wrap align-items-center">
                    <div class="sorting">
                        <select>
                            <option value="1">Default sorting</option>
                            <option value="1">Default sorting</option>
                            <option value="1">Default sorting</option>
                        </select>
                    </div>
                    <div class="sorting mr-auto">
                        <select>
                            <option value="1">Shoewe 12</option>
                            <option value="1">Shoewe 12</option>
                            <option value="1">Shoewe 12</option>
                        </select>
                    </div>
                    {{$produk->links()}}
                </div>

                <!-- End Filter Bar -->
                <!-- Start Best Seller -->
                <section class="lattest-product-area pb-40 category-list">
                    <div class="row">
                        <!-- single product -->
                        @foreach($produk as $row)
                            <div class="col-lg-4 col-md-6">
                                <div class="single-product">
                                    <a href="{{url('detail/'.$row->id_produk.'/detail')}}">
                                        <img class="img-fluid" src="{{url('uploads/').'/'.$row->gambar}}" alt="">
                                    </a>
                                    <div class="product-details">
                                        <h6>{{$row->nama_produk}}</h6>
                                        <div class="price">
                                            <h6>{{App\Http\Controllers\UserProdukController::rupiah($row->harga)}}</h6>
                                        </div>
                                        <div class="prd-bottom">
                                            <a class="social-info add-to-cart" data-id="{{$row->id_produk}}" role="button">
                                                <span class="ti-bag"></span>
                                                <p class="hover-text">add to bag</p>
                                            </a>
                                            <a href="" class="social-info">
                                                <span class="lnr lnr-heart"></span>
                                                <p class="hover-text">Wishlist</p>
                                            </a>
                                            <a href="{{url('detail/'.$row->id_produk.'/detail')}}" class="social-info">
                                                <span class="lnr lnr-move"></span>
                                                <p class="hover-text">view more</p>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </section>
                <!-- End Best Seller -->
                <!-- Start Filter Bar -->
                <div class="filter-bar d-flex flex-wrap align-items-center">
                    <div class="sorting mr-auto">
                        <select>
                            <option value="1">Show 12</option>
                            <option value="1">Show 12</option>
                            <option value="1">Show 12</option>
                        </select>
                    </div>
                    {{$produk->links()}}
                </div>
                <!-- End Filter Bar -->
            </div>
        </div>
    </div>
    <br>
    <br>
    <br>
    <br>
    <br>

    <script type="text/javascript" src="{{asset("js/vendor/jquery-2.2.4.min.js")}}"></script>
    <script type="text/javascript">
        $(function () {
            $(".add-to-cart").click(function (e) {
                // e.preventDefault();
                var ele = $(this);

                // ele.siblings('.btn-loading').show();

                $.ajax({
                    url: '{{ route('cart.add')}}',
                    method: "get",
                    data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id")},
                    dataType: "json",
                    success: function (response) {
                        // window.location.reload();
                        // ele.siblings('.btn-loading').hide();
                        alert("Berhasil Ditambah");
                        $(".hi").html(response.jumlah);

                        // $("span#status").html('<div class="alert alert-success">'+response.msg+'</div>');
                        // $("#header-bar").html(response.data);
                    },
                    error: function () {
                        alert("Gagal Memasukan Cart");
                    }
                });
            });
        });
    </script>

    <!-- Start related-product Area -->
    {{--    <section class="related-product-area section_gap">--}}
    {{--        <div class="container">--}}
    {{--            <div class="row justify-content-center">--}}
    {{--                <div class="col-lg-6 text-center">--}}
    {{--                    <div class="section-title">--}}
    {{--                        <h1>Deals of the Week</h1>--}}
    {{--                        <p></p>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--            <div class="row">--}}
    {{--                <div class="col-lg-9">--}}
    {{--                    <div class="row">--}}
    {{--                        @foreach($produk as $produk)--}}
    {{--                        <div class="col-lg-4 col-md-4 col-sm-6">--}}
    {{--                            <div class="single-related-product d-flex">--}}
    {{--                                <a href="{{url('detail/'.$produk->id_produk.'/detail')}}">--}}
    {{--                                    <img class="img-fluid" src="{{url('uploads/').'/'.$produk->gambar}}" alt="" style="width: 150px">--}}
    {{--                                </a>--}}
    {{--                                <div class="desc">--}}
    {{--                                    <a href="{{url('detail/'.$produk->id_produk.'/detail')}}" class="title">{{$produk->nama_produk}}</a>--}}
    {{--                                    <div class="price">--}}
    {{--                                        <h6>{{App\Http\Controllers\UserProdukController::rupiah($produk->harga)}}</h6>--}}
    {{--                                        <h6 class="l-through"></h6>--}}
    {{--                                    </div>--}}
    {{--                                </div>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                        @endforeach--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <div class="col-lg-3">--}}
    {{--                    <div class="ctg-right">--}}
    {{--                        <a href="#" target="_blank">--}}
    {{--                            <img class="img-fluid d-block mx-auto" src="img/category/c5.jpg" alt="">--}}
    {{--                        </a>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </section>--}}
    <!-- End related-product Area -->
@endsection