<link rel="stylesheet" href="{{asset('css/linearicons.css')}}">
<link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{asset('css/themify-icons.css')}}">
<link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
<link rel="stylesheet" href="{{asset('css/owl.carousel.css')}}">
<link rel="stylesheet" href="{{asset('css/nice-select.css')}}">
<link rel="stylesheet" href="{{asset('css/nouislider.min.css')}}">
<link rel="stylesheet" href="{{asset('css/ion.rangeSlider.css')}}" />
<link rel="stylesheet" href="{{asset('css/ion.rangeSlider.skinFlat.css')}}" />
<link rel="stylesheet" href="{{asset('css/magnific-popup.css')}}">
<link rel="stylesheet" href="{{asset('css/main.css')}}">

<!-- Start Banner Area -->
<section class="banner-area organic-breadcrumb">
    <div class="container">
        <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
            <div class="col-first">
                <h1>Invoice Page</h1>
                <nav class="d-flex align-items-center">
                    <a href="index.html">Home<span class="lnr lnr-arrow-right"></span></a>
                    <a href="#">Shop<span class="lnr lnr-arrow-right"></span></a>
                    <a href="single-product.html">Invoice Page</a>
                </nav>
            </div>
        </div>
    </div>
</section>
<!-- End Banner Area -->

<br>
<br>
<br>

<section class="ftco-section">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-7 ftco-animate">
                        @if(session()->has('success_message'))
                            <div class="alert alert-success" style="margin-left:3%;">
                                {{ session()->get('success_message') }}
                            </div>
                        @endif
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong> Perhatian </strong><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form action="{{route('checkout3.store')}}" method="POST">
                            {{ csrf_field() }}
                            <div class="row align-items-end">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="firstname">Nama Lengkap</label>
                                        <input type="text" class="form-control" name="name" value="{{ Auth::user()->name }}">
                                    </div>
                                </div>

                                <div class="w-100"></div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="country">Asal</label>
                                        <input type="text" class="form-control" name="asal" value="{{ $order->asal_kota }}" disabled>
                                    </div>
                                </div>
                                <div class="w-100"></div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="streetaddress">Tujuan</label>
                                        <input type="text" class="form-control" name="tujuan" placeholder="" value="{{ $order->tujuan_kota }}" disabled>
                                    </div>
                                </div>

                                <div class="w-100"></div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="towncity">Alamat Lengkap</label>
                                        <input type="text" class="form-control" name="alamat" placeholder="" >
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="postcodezip">Kode Pos</label>
                                        <input type="text" class="form-control" name="kode_pos" value="{{ $order->kodepos_tujuan }}" placeholder="" disabled>
                                    </div>
                                </div>
                                <div class="w-100"></div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="phone">No Telepon</label>
                                        <input type="text" class="form-control" name="phone" placeholder="">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="emailaddress">Email</label>
                                        <input type="text" class="form-control" name="email" placeholder="" value="{{ Auth::user()->email }}" disabled>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <input type="hidden" name="id_order" value="{{ $order->id_order }}">
                                    {{-- <!-- <input type="hidden" name="id_produk" value="{{ $order->produk }}"> --> --}}
                                    <input type="hidden" name="asal" value="{{ $order->asal_kota }}">
                                    <input type="hidden" name="tujuan" value="{{ $order->tujuan_kota }}">
                                    <input type="hidden" name="total_bayar" value="{{ $total + $order->tarif }}">
                                    <button class="btn btn-success text-center" type="submit" style="color:white; padding : 10px"><font color="white">Submit</font></button>

                                </div>
                        </form>


                    </div>



                    </form><!-- END -->
                </div>
                <div class="col-xl-5"  style="margin-top:-3.5%;">
                    <div class="row mt-5 pt-3" >
                        <div class="col-md-12 d-flex mb-5">
                            <div class="cart-detail cart-total p-3 p-md-4">
                                <h3 class="billing-heading mb-4">Total Pesanan</h3>
                                <?php $total = 0 ?>
                                @if(session('cart'))
                                    @foreach((array) session('cart') as $id => $details)
                                        <?php $total += $details['harga'] * $details['quantity'] ?>
                                        <p class="d-flex">
                                            <span><img src="{{ url('uploads/').'/'.$details['gambar'] }}" width="70px"></span>
                                            <span>{{ $details['nama_produk'] }}</span>
                                            <span>{{ $details['quantity'] }}</span>
                                        </p>

                                    @endforeach
                                @endif nav
                                <p class="d-flex">
                                    <span>Subtotal</span>
                                    <span>Rp. {{App\Http\Controllers\UserProdukController::rupiah($total)}} </span>
                                </p>
                                <p class="d-flex">
                                    <span>Onkos Kirim</span>
                                    <span>Rp. {{App\Http\Controllers\UserProdukController::rupiah($order->tarif)}}</span>
                                </p>

                                <hr>
                                <p class="d-flex total-price">
                                    <span>Total</span>
                                    <span>Rp. {{App\Http\Controllers\UserProdukController::rupiah($total + $order->tarif)}} </span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">

                    </div> <!-- .col-md-8 -->
                </div>
            </div>
        </section> <!-- .section -->

<script src="{{asset("js/vendor/jquery-2.2.4.min.js")}}"></script>
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"--}}
{{--        crossorigin="anonymous"></script>--}}
<script src="{{asset('js/vendor/bootstrap.min.js')}}"></script>
<script src="{{asset('js/jquery.ajaxchimp.min.js')}}"></script>
<script src="{{asset('js/jquery.nice-select.min.js')}}"></script>
<script src="{{asset('js/jquery.sticky.js')}}"></script>
<script src="{{asset('js/nouislider.min.js')}}"></script>
<script src="{{asset('js/countdown.js')}}"></script>
<script src="{{asset('js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('js/owl.carousel.min.js')}}"></script>
<script src="{{asset('js/components/shop_custom.js')}}"></script>
<!--gmaps Js-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>
<script src="{{asset('js/gmaps.min.js')}}"></script>