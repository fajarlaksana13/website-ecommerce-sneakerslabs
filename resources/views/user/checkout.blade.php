<link rel="stylesheet" href="{{asset('css/linearicons.css')}}">
<link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{asset('css/themify-icons.css')}}">
<link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
<link rel="stylesheet" href="{{asset('css/owl.carousel.css')}}">
<link rel="stylesheet" href="{{asset('css/nice-select.css')}}">
<link rel="stylesheet" href="{{asset('css/nouislider.min.css')}}">
<link rel="stylesheet" href="{{asset('css/ion.rangeSlider.css')}}" />
<link rel="stylesheet" href="{{asset('css/ion.rangeSlider.skinFlat.css')}}" />
<link rel="stylesheet" href="{{asset('css/magnific-popup.css')}}">
<link rel="stylesheet" href="{{asset('css/main.css')}}">

    <!-- Start Banner Area -->
    <section class="banner-area organic-breadcrumb">
        <div class="container">
            <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                <div class="col-first">
                    <h1>Estimate Page</h1>
                    <nav class="d-flex align-items-center">
                        <a href="index.html">Home<span class="lnr lnr-arrow-right"></span></a>
                        <a href="#">Shop<span class="lnr lnr-arrow-right"></span></a>
                        <a href="single-product.html">Estimate Page</a>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <!-- End Banner Area -->

    <br>
    <br>
    <br>
    <section class="ftco-section ftco-cart" >
        <div class="container">
            <div class="row justify-content-end">

                <div class="col-lg-8 mt-8 cart-wrap ftco-animate">
                    <div class="cart-total mb-3">
                        <h3>Estimate shipping</h3>
                        <p>Enter your destination and courier to get a shipping estimate</p>
                        <form action="{{route('checkout2.add')}}" method="POST" class="info" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="country">To</label>
                                <select name="destination" id="destination" class="form-control">
                                    <option selected="selected" value="">Choose Destination</option>
                                    @foreach($city as $c)
                                        <option value="{{ $c->id }}">{{ $c->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="country">Weight</label>
                                <input type="text" name="weight" value="{{$total_berat}}000" class="form-control text-left px-3">
                            </div>
                            <div class="form-group">
                                <label for="country">Kurir</label>
                                <select name="courier" id="courier" size="1" class="form-control">
                                    <option value="">Choose Courier</option>
                                    <option name="jne" value="jne">JNE</option>
                                    <option name="tiki" value="tiki">TIKI</option>
                                    <option name="pos" value="pos">POS</option>
                                </select>
                            </div>

                            <button type="submit" class="btn btn-primary py-3 px-4">Estimate</button>
                        </form>
                    </div>
                </div>
                <div class="col-lg-4 mt-5 cart-wrap ftco-animate">
                    <div class="cart-total mb-3" style="margin-left:2%;">
                        <h3>Details Cart</h3>
                        <?php $total = 0 ?>
                        @if(session('cart'))
                            @foreach((array) session('cart') as $id => $details)
                                <?php $total += $details['harga'] * $details['quantity'] ?>
                                <p class="d-flex">
                                    <span><img src="{{url('uploads/').'/'.$details['gambar']}}" width="70px"></span>
                                    <span>{{ $details['nama_produk'] }}</span>
                                    <span>{{ $details['quantity'] }} Kg</span>
                                </p>

                            @endforeach
                        @endif
                        <div class="cart-total mb-3">
                            <h3>Cart Totals</h3>
                            <p class="d-flex">
                                <span>Subtotal </span>
                                <span> Rp. {{App\Http\Controllers\UserProdukController::rupiah($total)}}	</span>
                            </p>
                            <hr>
                            <p class="d-flex total-harga">
                                <span>Total </span>
                                <span> Rp. {{App\Http\Controllers\UserProdukController::rupiah($total)}} </span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script src="{{asset("js/vendor/jquery-2.2.4.min.js")}}"></script>
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"--}}
    {{--        crossorigin="anonymous"></script>--}}
    <script src="{{asset('js/vendor/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/jquery.ajaxchimp.min.js')}}"></script>
    <script src="{{asset('js/jquery.nice-select.min.js')}}"></script>
    <script src="{{asset('js/jquery.sticky.js')}}"></script>
    <script src="{{asset('js/nouislider.min.js')}}"></script>
    <script src="{{asset('js/countdown.js')}}"></script>
    <script src="{{asset('js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('js/components/shop_custom.js')}}"></script>
    <!--gmaps Js-->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>
    <script src="{{asset('js/gmaps.min.js')}}"></script>