@extends('user.extend')

@section('extends')
<section class="ftco-section">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-7 ftco-animate">

                        <h3 class="mb-4 billing-heading">Shipping Details</h3>
                        <h5 class="mb-2 ">Dari: {{  $origin }} ({{ $origin_code }})</h5>
                        <h5 class="mb-2 ">Ke: {{ $destination }} ({{ $destination_code }})</h5>
                        <br/>


                        <table class="table table-striped table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Nama Layanan</th>
                                <th>Deskripsi</th>
                                <th>Tarif</th>
                                <th>ETD (Estimates Day)</th>
                                <th>&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach($array_result['rajaongkir']['results'] as $key => $value){
                            ?>
                            <h5><?php print $value['name'];?></h5>
                            <?php
                            foreach($value['costs'] as $keys => $values){
                            ?>
                            <tr>
                                <form action="{{route('checkout3.kirim')}}" method="POST" class="billing-form">
                                    <td>
                                        <?php print $values['service']; ?>
                                    </td>
                                    <td>
                                        <?php print $values['description']; ?>
                                    </td>
                                    <td>
                                        <?php print $values['cost'][0]['value']; ?>
                                    </td>
                                    <td>
                                        <?php print $values['cost'][0]['etd'];  ?> Hari
                                    </td>
                                    <td>

                                        {{ csrf_field() }}
                                        <input type="hidden" name="asal_kota" value="{{ $origin }}">
                                        <input type="hidden" name="kodepos_asal" value="{{ $origin_code }}">
                                        <input type="hidden" name="tujuan_kota" value="{{ $destination }}">
                                        <input type="hidden" name="kodepos_tujuan" value="{{ $destination_code }}">
                                        <input type="hidden" name="kurir" value="{{ $value['name'] }}">
                                        <input type="hidden" name="nama_layanan" value="{{ $values['service'] }}">
                                        <input type="hidden" name="deskripsi" value="{{ $values['description'] }}">
                                        <input type="hidden" name="tarif" value="{{ $values['cost'][0]['value'] }}">
                                        <input type="hidden" name="etd" value="{{ $values['cost'][0]['etd'] }}">
                                        <button type="submit" class="btn button-primary" style="background-color: #70c0b1">Pilih</button>
                                    </td>
                                </form>
                            </tr>
                            <?php
                            }
                            }
                            ?>
                            </tbody>

                        </table>


                        <div class="col-xl-5" >

                        </div>
                    </div> <!-- .col-md-8 -->
                </div>
            </div>
        </section> <!-- .section -->
 @endsection
