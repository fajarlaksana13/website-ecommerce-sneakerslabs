<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public $table = 'order';

    protected $primaryKey = 'id_order';

    protected $fillable = [
        'asal_kota','kodepos_asal','tujuan_kota','kodepos_tujuan','kurir','nama_layanan','deskripsi','tarif','etd'
    ];

    public function user(){
        return $this->belongsTo(App\User::class,'id');
    }

    public function produk(){
        return $this->belongsTo(App\Produk::class,'id_produk');
    }

    public function order(){
        return $this->belongsTo(App\Order::class,'id_order');
    }

    public function orders(){
        return $this->hasMany(App\DetailOrder::class,'id_order');
    }

}
