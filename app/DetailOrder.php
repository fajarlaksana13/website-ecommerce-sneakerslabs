<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailOrder extends Model
{
    public $table = 'detail_order';

    protected $primaryKey = 'id_detail';

    protected $fillable = [
        'id_order','id_user','id_produk','asal','tujuan','kode_pos','phone','total_bayar','alamat'
    ];

    public function user(){
        return $this->belongsTo(App\User::class,'id');
    }

    public function produk(){
        return $this->belongsTo(App\Produk::class,'id_produk');
    }

    public function order(){
        return $this->belongsTo(App\Order::class,'id_order');
    }

}
