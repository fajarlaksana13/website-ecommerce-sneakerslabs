<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class berita extends Model
{
    public $table = 'berita';

    protected $fillable = [
        'id_berita','judul','deskripsi','isi','gambar'
    ];
}
