<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    public $table = 'kategori';

    protected $fillable = [
        'id','nama_kategori','status'
    ];

    public function relasiProduk(){
        return $this->hasMany('App\Produk');
    }
}
