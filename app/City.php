<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public $table = 'city';

    protected $fillable = [
        'id','name','id_province'
    ];
}
