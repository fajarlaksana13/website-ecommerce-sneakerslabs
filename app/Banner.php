<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class banner extends Model
{
    public $table = 'banner';

    protected $fillable = [
        'id_banner','judul','deskripsi','gambar'
    ];
}
