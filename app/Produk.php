<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    public $table = 'produk';

    protected $primaryKey = 'id_produk';

    protected $fillable = [
        'nama_produk','deskripsi','gambar','harga','kategori_id','quantity'
    ];

    public function relasiKategori(){
        return $this->belongsTo('App\Kategori');
    }

    public function order(){
        return $this->hasMany(App\DetailOrder::class,'id_Produk');
    }
}
