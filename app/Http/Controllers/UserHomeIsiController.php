<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\banner;

class UserHomeIsiController extends Controller
{

    public function index(){
        $banner = DB::table('banner')->get();
        $produk = DB::table('produk')->orderBy('created_at','DESC')->paginate(8);
        $produk1 = DB::table('produk')->orderBy('created_at','ASC')->paginate(4);


        $data=array(
            'produk'=>$produk,'produk1'=>$produk1, 'banner'=>$banner
        );

        return view ('user.isi',$data);

    }

}
