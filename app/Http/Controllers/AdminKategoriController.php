<?php

namespace App\Http\Controllers;

use App\kategori;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;

class AdminKategoriController extends Controller
{
    public function index()
    {
        $kategori = Kategori::paginate(5);
        return view('admin.kategori',['kategori' => $kategori]);
    }


    public function create()
    {
        return view('kategori.form');
    }

    public function store(Request $request){


//        $input = $request->all();
//        $image = $request->file('cover');
//
//        $kategori = new \App\kategori;
//        $kategori->id = $input['id_berita'];
//        $berita->judul = $input['judul'];
//        $berita->deskripsi = $input['deskripsi'];
//        $berita->isi = $input['isi'];
//        if ($image != null) {
//            $image = $request->file('gambar');
//            $imagenameform = str_replace('', '_');
//            $imagename = pathinfo($imagenameform, 8);
//            $imageext = $request->file('gambar')->getClientOriginalExtension();
//            $imagenamestore = rand().'.'.$imageext;
//            $pathtostore = public_path('uploads');
//            $image->move($pathtostore, $imagenamestore);
//
//            $input['gambar'] = "{$imagenamestore}";
//        }
//        $status = $berita->save();
//        dd($request->hasFile('gambar'));



            $kategori = kategori::create(array(
                'nama_kategori' => Input::get('nama_kategori'),
                'status' => Input::get('status'),
            ));
            $kategori->save();



        return redirect()->route('kategori.index');

//        if ($status) {
        return redirect('/kategori')->with('success', 'Data Berhasil Ditambahkan');
//        } else {
//            return redirect('/tambahdatakategori')->with('error', 'Data Gagal Ditambahkan');
//        }
    }

    public function edit(Request $request, $id)
    {
        $data['kategori'] = \App\kategori::where('id', $id)->first();
        return view('kategori.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'id' => 'required',
            'nama_kategori' => 'required',
        ]);

        $input = $request->except('_token', '_method');
        $status = \App\kategori::where('id',$id);

        if($request->hasFile('gambar')){
            $image = $request->file('gambar');
            $imagenameext = $request->file('gambar')->getClientOriginalName();
            $imagenameform = str_replace('', '_', $imagenameext);
            $imagename = pathinfo($imagenameform, 8);
            $imageext = $request->file('gambar')->getClientOriginalExtension();
            $imagenamestore = rand().'.'.$imageext;
            $pathtostore = public_path('uploads');
            $image->move($pathtostore, $imagenamestore);

            $input['gambar'] = "{$imagenamestore}";

        }
        $status->update($input);
        if ($status) {
            return redirect('/kategori')->with('success', 'Data Berhasil Diubah');
        } else {
            return redirect('/kategori/edit')->with('error', 'Data Gagal Diubah');
        }

    }

    public function destroy(Request $request, $id)
    {
        $kategori = \App\kategori::where('id', $id);
        $status = $kategori->delete();


        if ($status) {
            return redirect('/kategori')->with('success', 'Data berhasil dihapus');
        } else {
            return redirect('/kategori')->with('error', 'Data gagal dihapus');
        }
    }

//    public function detail($id)
//    {
//        $data['kategori'] = \App\kategori::where('id', $id)->firstOrFail();
//        return \Illuminate\Support\Facades\View::make('user.detail')->with('kategori', $data);
//    }
}
