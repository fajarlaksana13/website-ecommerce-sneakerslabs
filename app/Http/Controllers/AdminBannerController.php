<?php

namespace App\Http\Controllers;

use App\Banner;
use App\berita;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;

class AdminBannerController extends Controller
{
    public function banner()
    {
        return view('admin.banner');
    }

    public function index(Request $request)
    {
        if ($request->has('cari')){
            $banner = banner::where('judul','LIKE','%'.$request->cari.'%')->paginate(5);
        }
        else{
            $banner = banner::orderBy('id_banner')->paginate(5);
        }
        return view('admin.banner',['banner'=>$banner]);
    }

    public function create()
    {
        return view('admin.banner.form');
    }

    public function store(Request $request){


//        $input = $request->all();
//        $image = $request->file('cover');
//
//        $banner = new \App\Banner;
//        $banner->id_banner = $input['id_banner'];
//        $banner->judul = $input['judul'];
//        $banner->deskripsi = $input['deskripsi'];
//        $banner->isi = $input['isi'];
//        if ($image != null) {
//            $image = $request->file('gambar');
//            $imagenameform = str_replace('', '_');
//            $imagename = pathinfo($imagenameform, 8);
//            $imageext = $request->file('gambar')->getClientOriginalExtension();
//            $imagenamestore = rand().'.'.$imageext;
//            $pathtostore = public_path('uploads');
//            $image->move($pathtostore, $imagenamestore);
//
//            $input['gambar'] = "{$imagenamestore}";
//        }
//        $status = $banner->save();
//        dd($request->hasFile('gambar'));
        if ($request->hasFile('gambar')) {
            $gambar = $request->file('gambar');
            $test = time() . '.' . $gambar->getClientOriginalExtension();
            Image::make($gambar)->save('uploads/'. $test);


            $banner = banner::create(array(
                'judul' => Input::get('judul'),
                'deskripsi' => Input::get('deskripsi'),
                'gambar' => $test
            ));
            $banner->save();
        }


        return redirect()->route('banner.index');

//        if ($status) {
        return redirect('/banner')->with('success', 'Data Berhasil Ditambahkan');
//        } else {
//            return redirect('/tambahdatabanner')->with('error', 'Data Gagal Ditambahkan');
//        }
    }

    public function edit(Request $request, $id_banner)
    {
        $data['banner'] = \App\Banner::where('id_banner', $id_banner)->first();
        return view('admin.banner.edit', $data);
    }

    public function update(Request $request, $id_banner)
    {
        $request->validate([
            'id_banner' => 'required',
            'judul' => 'required',
        ]);

        $input = $request->except('_token', '_method');
        $status = \App\Banner::where('id_banner',$id_banner);

        if($request->hasFile('gambar')){
            $image = $request->file('gambar');
            $imagenameext = $request->file('gambar')->getClientOriginalName();
            $imagenameform = str_replace('', '_', $imagenameext);
            $imagename = pathinfo($imagenameform, 8);
            $imageext = $request->file('gambar')->getClientOriginalExtension();
            $imagenamestore = rand().'.'.$imageext;
            $pathtostore = public_path('uploads');
            $image->move($pathtostore, $imagenamestore);

            $input['gambar'] = "{$imagenamestore}";

        }
        $status->update($input);
        if ($status) {
            return redirect('/banner')->with('success', 'Data Berhasil Diubah');
        } else {
            return redirect('/banner/edit')->with('error', 'Data Gagal Diubah');
        }

    }

    public function destroy(Request $request, $id_banner)
    {
        $banner = \App\Banner::where('id_banner', $id_banner);
        $status = $banner->delete();


        if ($status) {
            return redirect('/banner')->with('success', 'Data berhasil dihapus');
        } else {
            return redirect('/banner')->with('error', 'Data gagal dihapus');
        }
    }

    public function detail($id_banner)
    {
        $data['banner'] = \App\Banner::where('id_banner', $id_banner)->firstOrFail();
        return \Illuminate\Support\Facades\View::make('blog-singel')->with('banner', $data);
    }
}
