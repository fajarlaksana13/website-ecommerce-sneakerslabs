<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminUserController extends Controller
{
    public function user()
    {
        $data['users'] = \DB::table('users')->get();
        return view('admin.user', $data);
    }
}
