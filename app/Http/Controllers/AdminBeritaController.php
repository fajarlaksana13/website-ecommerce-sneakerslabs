<?php

namespace App\Http\Controllers;

use App\Produk;
use Illuminate\Http\Request;
use App\Berita;
use Faker\Provider\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;



class AdminBeritaController extends Controller
{
    public function berita()
    {
        return view('admin.berita');
    }
    public function index(Request $request)
    {
        if ($request->has('cari')){
            $berita = berita::where('judul','LIKE','%'.$request->cari.'%')->paginate(5);
        }
        else{
            $berita = berita::orderBy('id_berita')->paginate(5);
        }
        return view('admin.berita',['berita'=>$berita]);
    }


    public function create()
    {
        return view('Berita.form');
    }

    public function store(Request $request){


//        $input = $request->all();
//        $image = $request->file('cover');
//
//        $berita = new \App\berita;
//        $berita->id_berita = $input['id_berita'];
//        $berita->judul = $input['judul'];
//        $berita->deskripsi = $input['deskripsi'];
//        $berita->isi = $input['isi'];
//        if ($image != null) {
//            $image = $request->file('gambar');
//            $imagenameform = str_replace('', '_');
//            $imagename = pathinfo($imagenameform, 8);
//            $imageext = $request->file('gambar')->getClientOriginalExtension();
//            $imagenamestore = rand().'.'.$imageext;
//            $pathtostore = public_path('uploads');
//            $image->move($pathtostore, $imagenamestore);
//
//            $input['gambar'] = "{$imagenamestore}";
//        }
//        $status = $berita->save();
//        dd($request->hasFile('gambar'));
        if ($request->hasFile('gambar')) {
            $gambar = $request->file('gambar');
            $test = time() . '.' . $gambar->getClientOriginalExtension();
            Image::make($gambar)->save('uploads/'. $test);


            $berita = Berita::create(array(
                'judul' => Input::get('judul'),
                'deskripsi' => Input::get('deskripsi'),
                'isi' => Input::get('isi'),
                'gambar' => $test
        ));
                        $berita->save();
        }


        return redirect()->route('berita.index');

//        if ($status) {
            return redirect('/berita')->with('success', 'Data Berhasil Ditambahkan');
//        } else {
//            return redirect('/tambahdataBerita')->with('error', 'Data Gagal Ditambahkan');
//        }
    }

    public function edit(Request $request, $id_berita)
    {
        $data['berita'] = \App\Berita::where('id_berita', $id_berita)->first();
        return view('Berita.edit', $data);
    }

    public function update(Request $request, $id_berita)
    {
        $request->validate([
            'id_berita' => 'required',
            'judul' => 'required',
        ]);

        $input = $request->except('_token', '_method');
        $status = \App\berita::where('id_berita',$id_berita);

        if($request->hasFile('gambar')){
            $image = $request->file('gambar');
            $imagenameext = $request->file('gambar')->getClientOriginalName();
            $imagenameform = str_replace('', '_', $imagenameext);
            $imagename = pathinfo($imagenameform, 8);
            $imageext = $request->file('gambar')->getClientOriginalExtension();
            $imagenamestore = rand().'.'.$imageext;
            $pathtostore = public_path('uploads');
            $image->move($pathtostore, $imagenamestore);

            $input['gambar'] = "{$imagenamestore}";

        }
        $status->update($input);
        if ($status) {
            return redirect('/berita')->with('success', 'Data Berhasil Diubah');
        } else {
            return redirect('/berita/edit')->with('error', 'Data Gagal Diubah');
        }

    }

    public function destroy(Request $request, $id_berita)
    {
        $berita = \App\berita::where('id_berita', $id_berita);
        $status = $berita->delete();


        if ($status) {
            return redirect('/berita')->with('success', 'Data berhasil dihapus');
        } else {
            return redirect('/berita')->with('error', 'Data gagal dihapus');
        }
    }

    public function detail($id_berita)
    {
        $data['berita'] = \App\berita::where('id_berita', $id_berita)->firstOrFail();
        return \Illuminate\Support\Facades\View::make('user.blogIsi')->with('berita', $data);
    }
}
