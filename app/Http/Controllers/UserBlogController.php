<?php

namespace App\Http\Controllers;

use App\berita;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserBlogController extends Controller
{
//    public function index(){
//        $berita = DB::table('berita')->paginate(5);
//
//        return view ('user.blog', ['berita' => $berita]);
//    }
    public function index(Request $request)
    {
        if ($request->has('cariproduk')){
            $berita = berita::where('judul','LIKE','%'.$request->cariproduk.'%')->paginate(5);
        }
        else{
            $berita = berita::orderBy('id_berita')->paginate(5);
        }
        return view('user.blog',['berita'=>$berita]);
    }

}
