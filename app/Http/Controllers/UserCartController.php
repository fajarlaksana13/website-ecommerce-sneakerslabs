<?php

namespace App\Http\Controllers;

use App\City;
use Illuminate\Http\Request;
use App\Produk;

class UserCartController extends Controller
{
    public function index()
    {
//        $produk = \DB::table('produk')->get();
//        dd($produk);
        $city = City::get();

        return view('user.cart',['city'=>$city]);
    }

    public function addtocart(Request $request)
    {

        $id = $request->input('id');
        $produk = Produk::find($id);

        $cart = session()->get('cart');

        // if cart is empty then this the first produk
        if (!$cart) {
            $cart = [
                $id => [
                    "nama_produk" => $produk['nama_produk'],
                    "quantity" => 1,
                    "harga" => $produk['harga'],
                    "gambar" => $produk['gambar']
                ]
            ];

            session()->put('cart', $cart);

            $htmlCart = view('user.cart');

//            return redirect()->back();

            $jumlah = count(session('cart'));
            return response()->json(['data' => $htmlCart, 'jumlah' => $jumlah]);


        }

        // if cart not empty then check if this product exist then increment quantity
        if (isset($cart[$id])) {

            $cart[$id]['quantity']++;

            session()->put('cart', $cart);

            $htmlCart = view('user.cart');

//            return redirect()->back()->with('success', 'Product added to cart successfully!');
//            return redirect()->back();


            $jumlah = count(session('cart'));
            return response()->json(['data' => $htmlCart, 'jumlah' => $jumlah]);


        }

//        dd($produk['nama_produk']);
        // if item not exist in cart then add to cart with quantity = 1
        $cart[$id] = [
            "nama_produk" => $produk['nama_produk'],
            "quantity" => 1,
            "harga" => $produk['harga'],
            "gambar" => $produk['gambar']
        ];

        session()->put('cart', $cart);

        $htmlCart = view('user.cart');

//        return redirect()->back()->with('success', 'Product added to cart successfully!');
//        return redirect()->back();


        $jumlah = count(session('cart'));
        return response()->json(['data' => $htmlCart, 'jumlah' => $jumlah]);

    }

    public function remove(Request $request)
    {
        if($request->id_produk) {

            $cart = session()->get('cart');

            if(isset($cart[$request->id_produk])) {

                unset($cart[$request->id_produk]);

                session()->put('cart', $cart);
            }

//            $total = $this->getCartTotal();

            $htmlCart = view('user.cart');

            return response()->json(['data' => $htmlCart]);

            //session()->flash('success', 'Product removed successfully');
        }
    }

    public function removeall()
    {
        $hapus = session()->forget('cart');
        return redirect()->back();

    }
}
