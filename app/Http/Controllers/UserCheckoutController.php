<?php

namespace App\Http\Controllers;

use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use App\Province;
use App\City;
use App\DetailOrder;
use App\Order;
use auth;
use GuzzleHttp\Client;

class UserCheckoutController extends Controller
{
    public function checkout()
    {
        $total_berat = 0;

        $cart = session()->get('cart');

        foreach ($cart as $id => $details) {
            $total_berat += $details['quantity'];
        }
        $city = City::get();

        $data=array
        (
          'city'=>$city, 'cart'=>$cart, 'total_berat'=>$total_berat
        );

        return view('user.checkout',$data);
    }

    public function getprovince()
    {
        $client = new Client();

        try {
            $response = $client->get('https://api.rajaongkir.com/starter/province',
                array(
                    'headers' => array('key' => 'bc9eeeba437bfd0452c0a0019480a584')
                )
            );
            $json = $response->getBody()->getContents();
            $array_result = json_decode($json, true);
            for ($i = 0; $i < count($array_result["rajaongkir"]["results"]); $i++) {
                $province = new \App\Province;
                $province->id = $array_result["rajaongkir"]["results"][$i]["province_id"];
                $province->name = $array_result["rajaongkir"]["results"][$i]["province"];
                $province->save();
            }
        } catch (RequestException $e) {
            var_dump($e->getResponse()->getBody()->getContents());
        }


        //    print_r($array_result);
    }

    public function getcity()
    {
        $client = new Client();

        try {
            $response = $client->get('https://api.rajaongkir.com/starter/city',
                array(
                    'headers' => array('key' => 'bc9eeeba437bfd0452c0a0019480a584')
                )
            );
            $json = $response->getBody()->getContents();
            $array_result = json_decode($json, true);


            for ($i = 0; $i < count($array_result["rajaongkir"]["results"]); $i++) {
                $city = new \App\City;
                $city->id = $array_result["rajaongkir"]["results"][$i]["city_id"];
                $city->name = $array_result["rajaongkir"]["results"][$i]["city_name"];
                $city->kode_pos = $array_result["rajaongkir"]["results"][$i]["postal_code"];
                $city->id_province = $array_result["rajaongkir"]["results"][$i]["province_id"];
                $city->save();

            }
        } catch (RequestException $e) {
            var_dump($e->getResponse()->getBody()->getContents());
        }

    }

    public function prosesShipping(Request $request)
    {
        $client = new Client();

        try{
            $response = $client->request('POST','https://api.rajaongkir.com/starter/cost',
                [
                    'body' => 'origin=22&destination='.$request->destination.'&weight='.$request->weight.'&courier='.$request->courier.'',
                    'headers' => [
                        'key' => 'bc9eeeba437bfd0452c0a0019480a584',
                        'content-type'=> 'application/x-www-form-urlencoded',
                     ]
                ]
        );
            $json = $response->getBody()->getContents();
            $array_result = json_decode($json, true);
            $origin = $array_result["rajaongkir"]["origin_details"]["city_name"];
            $origin_code = $array_result["rajaongkir"]["origin_details"]["postal_code"];
            $destination = $array_result["rajaongkir"]["destination_details"]["city_name"];
            $destination_code = $array_result["rajaongkir"]["destination_details"]["postal_code"];
            $data=array(
                'origin'=>$origin,'origin_code'=>$origin_code,'destination'=>$destination,'destination_code'=>$destination_code,'array_result'=>$array_result
            );
            return view('user.checkout2', $data);
        } catch (RequestException $e){
            var_dump($e->getResponse()->getBody()->getContents());
        }


    }

    public function kirim(Request $request)
    {

        $order = new Order();
        $order->asal_kota = $request->input('asal_kota');
        $order->kodepos_asal = $request->input('kodepos_asal');
        $order->tujuan_kota = $request->input('tujuan_kota');
        $order->kodepos_tujuan = $request->input('kodepos_tujuan');
        $order->nama_layanan = $request->input('nama_layanan');
        $order->kurir = $request->input('kurir');
        $order->deskripsi = $request->input('deskripsi');
        $order->tarif = $request->input('tarif');
        $order->etd = $request->input('etd');
        $order->save();

        $total = 0;

        $cart = session()->get('cart');

        foreach($cart as $id => $details) {
            $total += $details['harga'] * $details['quantity'];
        }


        $data=array
        (
            'order'=>$order, 'total'=>$total, 'cart'=>$cart
        );


        return view ('user.checkout3', $data)->with('success_message', 'Item berhasil ditambahkan');

    }

    public function store(Request $request)
    {

        $det_order = new DetailOrder();
        $det_order->id_user = Auth::user()->id;
        $det_order->id_order = $request->input('id_order');
        $det_order->id_produk = $request->input('id_produk');
        $det_order->asal = $request->input('asal');
        $det_order->tujuan = $request->input('tujuan');
        $det_order->kode_pos = $request->input('kode_pos');
        $det_order->phone = $request->input('phone');
        $det_order->total_bayar = $request->input('total_bayar');
        $det_order->alamat = $request->input('alamat');

        $det_order->save();

        $order = \DB::table('order')
            ->join('detail_order', 'detail_order.id_order', '=', 'order.id_order')
            ->join('users', 'detail_order.id_user', '=','users.id')
            ->join('produk', 'detail_order.id_produk', '=','produk.id_produk')
            ->where('users.id','=', Auth::user()->id)
            ->select('order.*','produk.*', 'detail_order.*')
            ->first();

        $total = 0;

        $cart = session()->get('cart');

        foreach($cart as $id => $details) {
            $total += $details['harga'] * $details['quantity'];
        }

        $transaksi = DetailOrder::where('id_user', Auth::id() )->get();

        $data=array
        (
            'order'=>$order, 'det_order'=>$det_order, 'total'=>$total , 'transaksi'=>$transaksi
        );


        return view ('user.checkout3', $data)->with('success_message', 'Item berhasil ditambahkan');

    }
}
