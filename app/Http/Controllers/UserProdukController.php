<?php

namespace App\Http\Controllers;

use App\Produk;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserProdukController extends Controller
{
//    public function index(){
////        $produk = DB::table('produk')->paginate(6);
//        $produk = DB::table('produk')->orderBy('created_at','DESC')->paginate(6);
//
//        return view ('user.produk', ['produk' => $produk]);
//    }

    public function index(Request $request)
    {
        if ($request->has('cariproduk')){
            $produk = produk::where('nama_produk','LIKE','%'.$request->cariproduk.'%')->paginate(6);
        }
        else{
            $produk = produk::orderBy('id_produk')->paginate(6);
        }
        return view('user.produk',['produk'=>$produk]);
    }

    public static function rupiah($angka)
    {
        return 'Rp. '.strrev(implode('.',str_split(strrev(strval($angka)),3)));
    }



}
