<?php

namespace App\Http\Controllers;

use App\produk;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;

class AdminProdukController extends Controller
{
    public function index(Request $request)
    {
        if ($request->has('cari')){
            $produk = produk::where('nama_produk','LIKE','%'.$request->cari.'%')->paginate(5);
        }
        else{
            $produk = produk::orderBy('id_produk')->paginate(5);
        }
        return view('admin.produk',['produk'=>$produk]);
    }


    public function create()
    {
        return view('produk.form');
    }

    public function store(Request $request){


//        $input = $request->all();
//        $image = $request->file('cover');
//
//        $produk = new \App\produk;
//        $produk->id_produk = $input['id_berita'];
//        $berita->judul = $input['judul'];
//        $berita->deskripsi = $input['deskripsi'];
//        $berita->isi = $input['isi'];
//        if ($image != null) {
//            $image = $request->file('gambar');
//            $imagenameform = str_replace('', '_');
//            $imagename = pathinfo($imagenameform, 8);
//            $imageext = $request->file('gambar')->getClientOriginalExtension();
//            $imagenamestore = rand().'.'.$imageext;
//            $pathtostore = public_path('uploads');
//            $image->move($pathtostore, $imagenamestore);
//
//            $input['gambar'] = "{$imagenamestore}";
//        }
//        $status = $berita->save();
//        dd($request->hasFile('gambar'));
        if ($request->hasFile('gambar')) {
            $gambar = $request->file('gambar');
            $test = time() . '.' . $gambar->getClientOriginalExtension();
            Image::make($gambar)->save('uploads/'. $test);


            $produk = Produk::create(array(
                'nama_produk' => Input::get('nama_produk'),
                'deskripsi' => Input::get('deskripsi'),
                'kategori_id' => Input::get('kategori_id'),
                'harga' => Input::get('harga'),
                'gambar' => $test
            ));
            $produk->save();
        }


        return redirect()->route('produk.index');

//        if ($status) {
        return redirect('/Produk')->with('success', 'Data Berhasil Ditambahkan');
//        } else {
//            return redirect('/tambahdataProduk')->with('error', 'Data Gagal Ditambahkan');
//        }
    }

    public function edit(Request $request, $id_produk)
    {
        $data['produk'] = \App\produk::where('id_produk', $id_produk)->first();
        return view('produk.edit', $data);
    }

    public function update(Request $request, $id_produk)
    {
        $request->validate([
            'id_produk' => 'required',
            'nama_produk' => 'required',
        ]);

        $input = $request->except('_token', '_method');
        $status = \App\produk::where('id_produk',$id_produk);

        if($request->hasFile('gambar')){
            $image = $request->file('gambar');
            $imagenameext = $request->file('gambar')->getClientOriginalName();
            $imagenameform = str_replace('', '_', $imagenameext);
            $imagename = pathinfo($imagenameform, 8);
            $imageext = $request->file('gambar')->getClientOriginalExtension();
            $imagenamestore = rand().'.'.$imageext;
            $pathtostore = public_path('uploads');
            $image->move($pathtostore, $imagenamestore);

            $input['gambar'] = "{$imagenamestore}";

        }
        $status->update($input);
        if ($status) {
            return redirect('/produk')->with('success', 'Data Berhasil Diubah');
        } else {
            return redirect('/produk/edit')->with('error', 'Data Gagal Diubah');
        }

    }

    public function destroy(Request $request, $id_produk)
    {
        $produk = \App\produk::where('id_produk', $id_produk);
        $status = $produk->delete();


        if ($status) {
            return redirect('/produk')->with('success', 'Data berhasil dihapus');
        } else {
            return redirect('/produk')->with('error', 'Data gagal dihapus');
        }
    }

    public function detail($id_produk)
    {
//        $data['produk'] = \App\produk::where('id_produk', $id_produk)->firstOrFail();
        $data['produk'] = Produk::where('id_produk','=', $id_produk)->leftjoin('kategori','produk.kategori_id','=','kategori.id')->first();
        return view('user.detail')->with('produk', $data);
    }
}
