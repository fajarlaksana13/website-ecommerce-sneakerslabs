<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserDetailController extends Controller
{
    public function detail()
    {
        return view('user.detail');
    }
}
