<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserAboutController extends Controller
{
    public function about()
    {
        return view('user.about');
    }
}
